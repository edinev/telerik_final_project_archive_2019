# Team September

This is the repository for the final project of the QA cohort of team September
- Tsvetan Bratanov
- Emil Dinev

The links to the other tools which were used during our QA process are as follows
- [Trello](https://trello.com/b/ZN2JCmz2/social-network-qa-project)
- [TestRail](https://howtoqa.testrail.io/index.php?/suites/view/7&group_by=cases:section_id&group_order=asc)

We've added the object of our testing within our own repository as we have a
code freeze and no further work is done on the application.

The link to the PDFs of the Test Summary Report and to the Test Plan are listed below:
- [Test Summary Report](https://drive.google.com/open?id=1LNoHCU1STugvABF7EH7tYnTJn7DypopG)
- [Test Plan](https://drive.google.com/file/d/1--77GJXBaVMyBtxUYBbLgBdNHRho3nD4/view?usp=drivesdk)

Link to recorded videos of atomatisation:
- [Recorded videos](https://1drv.ms/u/s!Anx2IHMMJCMtjupIt3KEmFYUI_lbCg?e=4Z4hmT)