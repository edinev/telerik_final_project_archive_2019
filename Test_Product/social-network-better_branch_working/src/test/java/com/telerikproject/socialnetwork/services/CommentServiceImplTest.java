package com.telerikproject.socialnetwork.services;

import com.telerikproject.exceptions.CommentDoesNotExistException;
import com.telerikproject.exceptions.PostDoesNotExistException;
import com.telerikproject.models.Comment;
import com.telerikproject.models.Post;
import com.telerikproject.repositories.CommentRepository;
import com.telerikproject.services.CommentServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTest {

    private List<Comment> comments;

    @Mock
    CommentRepository commentRepository;

    @InjectMocks
    CommentServiceImpl commentService;

    @Before
    public void setUp() {
        comments = new ArrayList<>();
        comments.add(new Comment("content1"));
        comments.add(new Comment("content2"));
    }

    @Test
    public void getAllComments_Should_ReturnAllComments() {
        when(commentRepository.findAll()).thenReturn(comments);

        List<Comment> result = commentService.getAllComments();

        assertEquals(2, result.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getAllComments_When_Comment_NotExist() {
        commentService.getAllComments();
    }

    @Test
    public void getById_Should_ReturnComment_When_Comment_Exist() {
        int id = 0;

        when(commentRepository.findById(0)).thenReturn(java.util.Optional.of(new Comment("content")));

        final Comment result = commentService.getCommentById(id);

        assertEquals(0, result.getId());
    }

    @Test(expected = CommentDoesNotExistException.class)
    public void getById_Should_ReturnComment_When_Comment_NotExist() {
        commentService.getCommentById(6);
    }

    @Test
    public void delete_Should_CallRepositoryDelete_When_DeletingComment() {
        int id = 0;

        when(commentRepository.findById(0)).thenReturn(java.util.Optional.of(new Comment("content")));

        commentService.deleteComment(id);
    }

    @Test(expected = CommentDoesNotExistException.class)
    public void delete_Should_ThrowEntityNotFoundException_When_DeletingPost() {
        // Arrange
        Comment comment = new Comment("content");

        // Act
        commentService.deleteComment(1);
    }
//    @Test
//    public void update_Should_CallRepositoryUpdate_When_UpdatingPost() {
//        // Arrange
//        Comment comment = new Comment("TestContent");
//
//        // Act
//        commentService.editComment(1, "content");
//
//        // Assert
//        Mockito.verify(commentRepository, Mockito.times(1)).save(comment);
//    }

//    @Test(expected = EntityNotFoundException.class)
//    public void delete_Should_CallRepositoryDelete_When_CommentNotExist() {
//        int id = 0;
//
//        when(commentRepository.findById(0)).thenReturn(null);
//
//        commentService.deleteComment(id);
//    }
}