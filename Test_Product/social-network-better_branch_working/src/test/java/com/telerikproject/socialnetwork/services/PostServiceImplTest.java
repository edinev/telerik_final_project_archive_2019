package com.telerikproject.socialnetwork.services;

import com.telerikproject.DTOs.PostDTO;
import com.telerikproject.exceptions.PostDoesNotExistException;
import com.telerikproject.exceptions.UserDoesNotExistException;
import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import com.telerikproject.repositories.PostRepository;
import com.telerikproject.services.PostServiceImpl;
import javafx.geometry.Pos;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


import java.security.Principal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceImplTest {

    @Mock
    PostRepository postRepository;

    @InjectMocks
    PostServiceImpl postService;

    @Test
    public void getAllPublicPostsOrderByPostDateDesc_Should_ReturnAllPublicPostsOrderByPostDateDesc() {
        // Arrange
        Mockito.when(postService.getAllPublicPostsOrderByPostDateDesc()).thenReturn(Arrays.asList(
                new Post("TestContent1"),
                new Post("TestContent2"),
                new Post("TestContent3")
        ));

        // Act
        List<Post> result = postService.getAllPublicPostsOrderByPostDateDesc();

        // Assert
        Assert.assertEquals(3, result.size());
    }

    @Test
    public void getAllUserPosts_Should_ReturnAllUserPosts() {
        // Arrange
        Mockito.when(postService.getAllUserPostsById(1)).thenReturn(Collections.singletonList(
                new Post("TestContent")
        ));

        // Act
        List<Post> result = postService.getAllUserPostsById(1);

        // Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getById_Should_ReturnPost_When_Post_Exist() {
        final int id = 0;

        when(postRepository.findById(0)).thenReturn(java.util.Optional.of(new Post("content")));

        final Post result = postService.getPostById(id);

        assertEquals(0, result.getId());
    }

    @Test(expected = PostDoesNotExistException.class)
    public void getById_Should_ThrowException_When_User_NotExist() {
        // Arrange
        Post post = new Post("content");

        // Act
        postService.getPostById(2);
    }

    @Test(expected = PostDoesNotExistException.class)
    public void delete_Should_ThrowEntityNotFoundException_When_DeletingPost() {
        // Arrange
        Post post = new Post("content");

        // Act
        postService.deletePost(1);
    }

//    @Test
//    public void delete_Should_CallRepositoryDelete_When_DeletingPost() {
//        // Arrange
//        Post post = new Post("content");
//
//        // Act
//        Mockito.when(postRepository.findById(1)).thenReturn(Optional.of(post));
//        postService.deletePost(1);
//
//        // Assert
//        Mockito.verify(postRepository, Mockito.times(1)).delete(post);
//    }

//    @Test
//    public void delete_Should_CallRepositoryUpdate_When_DeletingUser() {
//        // Arrange
//        Post post = new Post("content");
//
//        // Act
//        postService.deletePost(1);
//
//        // Assert
//        Mockito.verify(postRepository, Mockito.times(1)).delete(post);
//    }

//    @Test
//    public void update_Should_CallRepositoryUpdate_When_UpdatingPost() {
//        // Arrange
//        Post post = new Post("content");
//
//        // Act
//        postService.editPost(1, post);
//
//        // Assert
//        Mockito.verify(postRepository, Mockito.times(1)).save(post);
//    }


//    @Test
//    public void update_Should_CallRepositoryUpdate_When_UpdatingPost() {
//        // Arrange
//        Post post = new Post("TestContent");
//
//        // Act
//        postService.editPost(1, post);
//
//        // Assert
//        Mockito.verify(postRepository, Mockito.times(1)).save(post);
//    }
//
//    @Test
//    public void create_Should_CallRepository_When_Create_NewPost() {
//        // Arrange
//        PostDTO post = new PostDTO("TestContent");
//
//        // Act
//        postService.createPost(post, null);
//
//        // Assert
//        Mockito.verify(postRepository, Mockito.times(1))
//                .save(post);
//    }

    //    @Test
//    public void testCreateAllPostsShouldCreatePost() {
//
//        when(postRepository.saveAndFlush(any(Post.class))).thenReturn( new Post("content"));
//        Post newPost = postService.createPost(new PostDTO("content"), new User("user", "pass"));
//
//        assertEquals("sdcf", newPost.getContent());
//    }

}