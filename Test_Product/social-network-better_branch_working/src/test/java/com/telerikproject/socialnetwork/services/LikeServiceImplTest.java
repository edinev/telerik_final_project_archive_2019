package com.telerikproject.socialnetwork.services;

import com.telerikproject.models.Like;
import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import com.telerikproject.repositories.LikeRepository;
import com.telerikproject.repositories.PostRepository;
import com.telerikproject.repositories.UserRepository;
import com.telerikproject.services.LikeServiceImpl;
import com.telerikproject.services.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class LikeServiceImplTest {

    private List<Like> likes;

    @Mock
    LikeRepository likeRepository;

    @Mock
    PostRepository postRepository;

    @Mock
    UserRepository userRepository;

    @Mock
    UserServiceImpl userService;

    @Mock
    Principal principal;

    @InjectMocks
    LikeServiceImpl likeService;

    @Before
    public void setUp() {
        likes = new ArrayList<>();
        likes.add(new Like(true));
        likes.add(new Like(true));
    }

    @Test
    public void getAllLikes_Should_ReturnAllLikes() {
        when(likeRepository.findAll()).thenReturn(likes);

        final List<Like> result = likeService.getAllLikes();

        assertEquals(2, result.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getAllLikes_When_Comment_NotExist() {
        likeService.getAllLikes();
    }

    @Test
    public void getByPostId_Should_ReturnLike_When_Like_Exist() {
        int id = 0;

        when(likeRepository.getAllByPostId(0)).thenReturn(likes);

        final List<Like> result = likeService.getAllLikesByPostId(id);

        assertEquals(2, result.size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getById_Should_ReturnLike_When_Like_NotExist() {
        likeService.getLikeById(6);
    }

    @Test
    public void getById_Should_ReturnLike_When_Like_Exist() {
        int id = 0;

        when(likeRepository.getLikeByIdAndIsLikedTrue(0)).thenReturn(new Like(true));

        final Like result = likeService.getLikeById(id);

        assertEquals(true, result.getLiked());
    }

//    @Test(expected = EntityNotFoundException.class)
//    public void getByPostId_Should_ReturnLike_When_Like_NotExist() {
//        likeService.getAllLikesByPostId(6);
//    }

    @Test
    public void delete_Should_CallRepositoryDelete_When_DeletingLike() {
        int id = 0;

        when(likeRepository.getLikeByIdAndIsLikedTrue(0)).thenReturn(new Like(true));

        likeService.deleteLikeById(id);
    }

    @Test(expected = EntityNotFoundException.class)
    public void delete_Should_CallRepositoryDelete_When_LikeNotExist() {
        int id = 0;

        when(likeRepository.getLikeByIdAndIsLikedTrue(0)).thenReturn(null);

        likeService.deleteLikeById(id);
    }
//
//    @Test
//    public void likePost_Should_AddLike(){
//
//        Post mockPost = new Post();
//        User mockUser = new User();
//
//        Mockito.when(postRepository.findById(1)).thenReturn(java.util.Optional.of(mockPost));
//        Mockito.when(principal.getName()).thenReturn("Ivan");
//        Mockito.when(userService.getUserByUsername("Ivan")).thenReturn(mockUser);
//
//        likeService.likeAndDislikePost(1, principal);
//
//        Mockito.verify(postRepository, Mockito.times(1)).save(mockPost);
//    }
}