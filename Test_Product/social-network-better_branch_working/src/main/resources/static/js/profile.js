$(document).ready(function () {

    let userIdElement = document.getElementById("userId");

    "use strict";
    const url = "http://localhost:8080/api/posts/profile/all";
    $(function () {
        createComment();
        getPosts(url);
        getCurrentUserFriends()
    });
});

function getCurrentUserFriends() {
    let url = '/api/connection/currentUserFriends';

    $.ajax({
        url: url,
        type: "GET",
        async: true,
        success: function (data) {
            let allUsers = Object.values(data);

            for (let i = 0; i < allUsers.length; i++) {
                let userObj = `
        <li>
        <a data-toggle="tooltip" title="${allUsers[i].firstName} ${allUsers[i].lastName}" class="thumbnail" href="/user/${allUsers[i].id}">
        <img style="
    position: relative;
    width:  50px;
    height: 50px;
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;
    cursor: pointer;
" src="img/${allUsers[i].userDetails.imagePath}"/>
        </a>
        </li>`;

                $("#friends").append(userObj);
            }
        },
        error: function () {
            console.log('error in getting currentUserFriends(Try logging in)')
        }
    });
}
