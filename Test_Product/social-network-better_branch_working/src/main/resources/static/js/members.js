$(document).ready(function () {
    const url = "http://localhost:8080/api/users/all";
    getUsers(url);

    function getUsers(url) {
        $.ajax({
            url: url,
            type: "GET",
            async: true,
            success: function (data) {
                generateUsers(data)
            },
            error: function () {
                console.log('error in displaying all posts')
            }
        });
    }


    $(function () {
        $('#search').keyup(function () {
            debugger
            let val = $(this).val().toLowerCase();
            // let textElement = this.getElementsByClassName("text-center");

            $(".names").hide();

            $(".names").each(function () {


                let text = $(this).text().toLowerCase();

                if (text.indexOf(val) !== -1) {

                    $(this).show();
                }
            });
        });
    });


});

function generateUsers(users) {
    for (let i = 0; i < users.length; i++) {

        const userId = users[i].id;
        debugger

        if (isUserLogged()) {
            let user = getCurrentUser();
            if (user[0] === userId) {
                continue
            }
        }

        let userObj =
            `<div class="names" id="names-${userId}">
        <div class="row member-row">
                <div class="col-md-3">
                <img src="/img/${users[i].userDetails.imagePath}" class="img-thumbnail"/> <!-- bootstrap class - smaller with border and padding -->
                <div style="display: none;">${userId}</div>
            <div class="text-center">${users[i].firstName} ${users[i].lastName}</div>
                </div> <!-- col 3 -->
                <div class="col-md-3">
                <p class="hideForNotAuthorizedUser"><a type="button" id="send-${userId}" onclick="sendFriendRequest(${userId})" class="btn btn-success btn-block"><i class="fa fa-users"></i> Add Friend</a></p>
            </div> <!-- col 3 -->
            <div class="col-md-3">
                <p><a id="delete-${userId}" style="visibility: hidden;" class="btn btn-default btn-block"><i class="fa fa-envelope"></i> Delete Friend</a></p>
            </div> <!-- col 3 -->
            <div class="col-md-3">
                <p><a id="user${userId}" href="/user/" class="btn btn-primary btn-block"><i class="fa fa-edit"></i> View Profile</a></p>
            </div> <!-- col 3 -->
            </div> <!-- row member-row -->
            </div>`;

        if (isUserLogged()) {
            removeFriendButtonForAlreadyFriends(userId);
            addFriendButton(userId);
            acceptFriend(userId);
        }


        $("#userObj").append(userObj);

        let profileElement = document.getElementById('user' + userId);

        profileElement.href += userId;
    }

}

function removeFriendButtonForAlreadyFriends(userId) {
    let url = "/api/connection/currentUserFriends";
    $.ajax({
        url: url,
        type: "GET",
        async: true,
        success: function (friends) {
            if (friends !== []) {
                let likeButtonElement = document.getElementById('send-' + userId);
                for (let i = 0; i < friends.length; i++) {
                    if (friends[i].id === userId) {
                        likeButtonElement.style.display = "none";
                        let deleteButtonElement = document.getElementById('delete-' + userId);
                        deleteButtonElement.style.visibility = "visible";
                        deleteButtonElement.setAttribute("onclick", "deleteFriendRequest(" + userId + "), window.location.reload()")
                    }
                }
            }
        },
        error: function () {
            console.log("error")
        }
    });
}

function removeFriend(userId) {
    let url = '/api/connection/accept/' + userId;
    $.ajax({
        url: url,
        type: "PUT",
        async: true,
        success: function () {
            console.log('friend request accepted');
        },
        error: function () {
            console.log('error in getting currentUser(Try logging in)')
        }
    });
}

function acceptFriend(userId) {
    let url = "/api/connection/getFriendRequests";
    $.ajax({
        url: url,
        type: "GET",
        async: true,
        success: function (friendRequests) {
            if (friendRequests !== []) {
                let likeButtonElement = document.getElementById('send-' + userId);
                let deleteButtonElement = document.getElementById('delete-' + userId);
                debugger
                for (let i = 0; i < friendRequests.length; i++) {
                    if (friendRequests[i].id === userId) {
                        likeButtonElement.textContent = " Accept Request";
                        likeButtonElement.setAttribute("onclick", "acceptFriendRequest(" + userId + "), window.location.reload()");
                        deleteButtonElement.style.visibility = "visible";
                        deleteButtonElement.textContent = " Decline Request";
                        likeButtonElement.addEventListener("mouseenter", ev => {
                            deleteButtonElement.setAttribute("click", "declineFriendRequest(" + userId + ")")
                        });
                    }
                }
            }
        },
        error: function () {
            console.log("error")
        }
    });
}

function declineFriendRequest(userId) {
    debugger
    let url = '/api/connection/decline/' + userId;
    $.ajax({
        url: url,
        type: "PUT",
        async: true,
        success: function () {
            console.log('friend request denied');
        },
        error: function () {
            console.log('error in getting currentUser(Try logging in)')
        }
    });
}

function acceptFriendRequest(userId) {
    let url = '/api/connection/accept/' + userId;
    $.ajax({
        url: url,
        type: "PUT",
        async: true,
        success: function () {
            console.log('friend request accepted');
        },
        error: function () {
            console.log('error in getting currentUser(Try logging in)')
        }
    });
}

function addFriendButton(userId) {
    let url = "/api/connection/getSendFriendRequests";
    $.ajax({
        url: url,
        type: "GET",
        async: true,
        success: function (sendRequests) {
            if (sendRequests !== []) {

                let likeButtonElement = document.getElementById('send-' + userId);
                debugger
                for (let i = 0; i < sendRequests.length; i++) {
                    if (sendRequests[i].secondUser.id === userId) {
                        likeButtonElement.textContent = " Pending Request";
                        likeButtonElement.addEventListener("mouseenter", ev => {
                                likeButtonElement.textContent = ' Cancel Request';
                                likeButtonElement.style.backgroundColor = 'red';
                                likeButtonElement.style.cursor = 'pointer';
                                likeButtonElement.setAttribute("onclick", 'deleteFriendRequest(' + userId + ')')
                            }
                        );
                        likeButtonElement.addEventListener("mouseleave", ev => {
                            likeButtonElement.textContent = ' Pending Request';
                            likeButtonElement.style.backgroundColor = '#5cb85c';
                        })
                    }
                }
            }
        },
        error: function () {
            console.log('error in displaying all likes')
        }
    });
}

function deleteFriendRequest(userId) {
    debugger

    let url = "/api/connection/delete/" + userId;

    $.ajax({
        url: url,
        type: "DELETE",
        async: true,
        success: function () {
            getUsers('/api/users/all')
        },
        error: function () {
            console.log('error in getting currentUser(Try logging in)')
        }
    });
}

function sendFriendRequest(id) {
    let url = "/api/connection/send-request/" + id;

    $.ajax({
        url: url,
        type: "POST",
        async: true,
        success: function (data) {
            addFriendButton(id);
        },
        error: function () {
            console.log('error in getting currentUser(Try logging in)')
        }
    });
}
