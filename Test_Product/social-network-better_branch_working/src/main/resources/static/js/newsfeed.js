$(document).ready(function () {
    "use strict";
    const url = "http://localhost:8080/api/posts/allPublic";
    const usersUrl = "http://localhost:8080/api/users/all";
    $(function () {
        createComment();
        getPosts(url);
        getRegisteredUsers(usersUrl);
    });

});