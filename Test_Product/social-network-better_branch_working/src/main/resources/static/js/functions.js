function getRegisteredUsers(url) {
    $.ajax({
        url: url,
        type: "GET",
        async: true,
        success: function (data) {
            generateRegisteredUsers(data)
        },
        error: function () {
            console.log('error in displaying all posts')
        }
    });
}

function generateRegisteredUsers(users) {
    let allUsers = Object.values(users);


    for (let i = 0; i < allUsers.length; i++) {
        let userObj = `
        <li>
        <a data-toggle="tooltip" title="${allUsers[i].firstName} ${allUsers[i].lastName}" class="thumbnail" href="/user/${allUsers[i].id}">
        <img style="
    position: relative;
    width:  50px;
    height: 50px;
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;
    cursor: pointer;
" src="img/${allUsers[i].userDetails.imagePath}"/>
        </a>
        </li>`;

        $("#registered-users").append(userObj);
    }
}

function createComment() {
    $("#postObj").on("click", "#commentButton", function () {
        if (!isUserLogged()) {
            alert("You must login before commenting!");
            return;
        }
        debugger
        let $postId = $(this).closest(".panel-default");
        let $text = $(this).parent().find("#comment-content").val();

        if ($text === '' || $text === null) {
            alert("Comment can't be empty!");
            return false;
        }

        console.log($postId.attr("id"));
        console.log($text);

        let obj = {
            "postId": $postId.attr("id"),
            "userId": "1",
            "content": $text
        };

        fetch("/comments/createComment", {
            method: "POST",
            body: JSON.stringify(obj),
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json, text/plain, */*"
            },

        })
            .then(function () {
                console.log("then");
                refreshCurrentPost($postId.attr("id"));
            }).catch(function (error) {
            console.error("Create comment error: " + error);
        });

        $("#comment-put-" + $postId).prepend()
    });

}


function getPosts(url) {
    $.ajax({
        url: url,
        type: "GET",
        async: true,
        success: function (data) {
            generatePosts(data)
        },
        error: function () {
            console.log('error in displaying all posts')
        }
    });
}

function generatePosts(posts) {

    for (let i = 0; i < posts.length; i++) {
        let likes;
        let postId = posts[i].id;
        let postObj;


        if (posts[i].likes.length === undefined) {
            likes = 0;
        } else {
            likes = posts[i].likes.length
        }
        postObj =
            `
            <div id="${postId}" class="panel panel-default post">
            <div class="panel-body">
            <div class="row">
            <div class="col-sm-2">
            <a class="post-avatar thumbnail" href="/profile">
                <img src="/img/${posts[i].user.userDetails.imagePath}"/>
                
                    <div class="text-center">
                        ${posts[i].user.firstName} ${posts[i].user.lastName}
                    </div>
            </a>
        <div class="likes text-center"><span id="refresh-this-${postId}"></span> Likes</div> 
        </div> <!-- col sm 2 -->

        <div class="col-sm-10">
            <div class="bubble">
            <div class="pointer">
            <span>${posts[i].content}</span>
        </div> <!-- pointer -->
        <div class="pointer-border"></div>
            </div> <!-- bubble -->
            <p class="post-actions" id="like-append-${postId}">
            <a style="cursor: pointer" type="button" onclick="likePost(${postId})"><span id="like-${postId}"></span></a>
            </p> <!-- post-actions -->
            <div class="comment-form" id="comment-put-${postId}">
            <form class="form-inline">
            <div class="form-group">
            <input  th:action="@{/comments}" name="content"  type="text" class="form-control" id="comment-content" placeholder="Enter Comment">
            </div>
            <button id="commentButton"  type="button" class="btn btn-default">Add</button>
            </form>
            </div> <!-- comment-form -->
            <div class="clearfix"></div> <!-- to clear all floats -->
            </div> <!-- col sm 10 -->
            </div> <!-- row -->
            </div> <!-- panel body -->
            </div> <!-- panel -->`;

        let isLogged = isUserLogged();

        if (isLogged) {
            getLikeButton(postId, getCurrentUser())
        } else {
            getLikeCounterForUnloggedUsers(postId);
        }

        $("#postObj").append(postObj);
        let commentObj;

        for (let j = 0; j < posts[i].comments.length; j++) {
            commentObj = `
            <div class="comments">
            <div class="comment">
            <div data-toggle="tooltip" title="${posts[i].comments[j].user.firstName} ${posts[i].comments[j].user.lastName}">
            <a href="/user/${posts[i].comments[j].user.id}" class="comment-avatar pull-left">
            <img src="/img/${posts[i].comments[j].user.userDetails.imagePath}"/>
            </a>
            <div class="comment-text" id="commentContent">
            ${posts[i].comments[j].content}
        </div> <!-- comment-text -->
        </div> <!-- comment -->
        <div class="clearfix"></div> <!-- clear floats -->
            </div> <!-- comments -->`;
            $("#comment-put-" + postId).append(commentObj);


        }
    }

}

function refreshCurrentPost(postId) {
    debugger
    let url = "/api/posts/" + postId;
    $.ajax({
        url: url,
        type: "GET",
        async: true,
        success: function (post) {
            let postObjElement = document.getElementById(post.id);
            postObjElement.innerHTML = '';

            postObjElement.innerHTML = `
           
            <div class="panel-body">
            <div class="row">
            <div class="col-sm-2">
            <a class="post-avatar thumbnail" href="/profile">
                <img src="/img/${post.user.userDetails.imagePath}"/>
                
                    <div class="text-center">
                        ${post.user.firstName} ${post.user.lastName}
                    </div>
            </a>
        <div class="likes text-center"><span id="refresh-this-${post.id}"></span> Likes</div> 
        </div> <!-- col sm 2 -->

        <div class="col-sm-10">
            <div class="bubble">
            <div class="pointer">
            <span>${post.content}</span>
        </div> <!-- pointer -->
        <div class="pointer-border"></div>
            </div> <!-- bubble -->
            <p class="post-actions" id="like-append-${post.id}">
            <a style="cursor:pointer" type="button" onclick="likePost(${post.id})"><span id="like-${post.id}"></span></a>
            </p> <!-- post-actions -->
            <div class="comment-form" id="comment-put-${post.id}">
            <form class="form-inline">
            <div class="form-group">
            <input  th:action="@{/comments}" name="content"  type="text" class="form-control" id="comment-content" placeholder="Enter Comment">
            </div>
            <button id="commentButton" type="button" class="btn btn-default">Add</button>
            </form>
            </div> <!-- comment-form -->
            <div class="clearfix"></div> <!-- to clear all floats -->
            </div> <!-- col sm 10 -->
            </div> <!-- row -->
            </div> <!-- panel body -->`;

            let isLogged = isUserLogged();
            debugger
            if (isLogged) {
                getLikeButton(post.id, getCurrentUser());
            } else {
                getLikeCounterForUnloggedUsers(post.id);
            }

            let commentObj;

            for (let j = 0; j < post.comments.length; j++) {
                commentObj = `
            <div class="comments">
            <div class="comment">
            <div data-toggle="tooltip" title="${post.comments[j].user.firstName} ${post.comments[j].user.lastName}">
            <a href="/user/${post.comments[j].user.id}" class="comment-avatar pull-left">
            <img src="/img/${post.comments[j].user.userDetails.imagePath}"/>
            </a>
            <div class="comment-text" id="commentContent">
            ${post.comments[j].content}
        </div> <!-- comment-text -->
        </div> <!-- comment -->
        <div class="clearfix"></div> <!-- clear floats -->
            </div> <!-- comments -->`;
                $("#comment-put-" + post.id).append(commentObj);
            }
        },
        error: function () {
            console.log("isUserLogged error")
        }
    });
}

function isUserLogged() {
    let url = "/api/users/isAuthorized";
    let returnValue = null;
    $.ajax({
        url: url,
        type: "GET",
        async: false,
        success: function (data) {
            let value = Object.values(data);
            returnValue = value[0];
        },
        error: function () {
            console.log("isUserLogged error")
        }
    });
    return returnValue;
}

function validatePostForm() {
    debugger
    let content = document.forms["post-form"]["content"].value;
    if (content === "" || content === null) {
        alert("Post content can't be empty!");
        return false;
    }
}


function getCurrentUser() {
    let url = "/api/users/currentUser";
    let user = null;
    $.ajax({
        url: url,
        type: "GET",
        async: false,
        success: function (data) {
            user = Object.values(data);
        },
        error: function () {
            console.log('error in getting currentUser(Try logging in)')
        }
    });
    return user;
}

function getLikeCounterForUnloggedUsers(postId) {
    let url = "/api/likes/post/" + postId;
    debugger
    $.ajax({
        url: url,
        type: "GET",
        async: true,
        success: function (likes) {
            let likeCounter = document.getElementById('refresh-this-' + postId);
            likeCounter.textContent = likes.length;
        },
        error: function () {
            console.log('error in displaying all likes')
        }
    });
}

function getLikeButton(postId, currentUser) {
    let url = "/api/likes/post/" + postId;
    $.ajax({
        url: url,
        type: "GET",
        async: true,
        success: function (likes) {
            let likeElem = document.getElementById('like-' + postId);
            let likeCounter = document.getElementById('refresh-this-' + postId);
            if (likes.length === 0) {
                likeElem.textContent = "Like";
                likeCounter.textContent = likes.length;
            } else {
                for (let i = 0; i < likes.length; i++) {
                    debugger
                    if (likes[i].user.id === currentUser[0]) {
                        likeElem.textContent = "Unlike";
                        likeCounter.textContent = likes.length;
                        return;
                    } else {
                        likeElem.textContent = "Like";
                        likeCounter.textContent = likes.length;
                    }
                }
            }
        },
        error: function () {
            console.log('error in displaying all likes')
        }
    });
}

function likePost(postId) {

    $.ajax(
        {
            url: `/api/posts/${postId}/like`,
            type: 'PUT',
            async: true,
            success: function () {
                getLikeButton(postId, getCurrentUser())
            },
            error: function () {
                console.log("error in liking post with id " + postId);
            }
        }
    )
}



