package com.telerikproject.controllers.rest;

import com.telerikproject.DTOs.CommentDTO;
import com.telerikproject.models.Comment;
import com.telerikproject.services.interfaces.CommentService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/comments")
public class CommentRestController {

    private CommentService commentService;

    private UserService userService;

    @Autowired
    public CommentRestController(CommentService commentService, UserService userService) {
        this.commentService = commentService;
        this.userService = userService;
    }

    @PostMapping("/createComment")
    public void createComment(@Valid @RequestBody CommentDTO commentDTO,
                              @RequestParam(name = "page", required = false) String page,
                              Principal principal) {
        try {
            commentService.createComment(commentDTO, userService.getCurrentUser(principal));
        }
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/all")
    public List<Comment> getAll() {
        try {
            return commentService.getAllComments();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Comment getCommentById(@PathVariable Integer id) {
        try {
            return commentService.getCommentById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/edit/{comment}")
    public Comment editComment(@PathVariable Integer id, @PathVariable Comment comment) {
        try {
            return commentService.editComment(id, comment);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteComment(@PathVariable Integer id) {
        try {
            commentService.deleteComment(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
