package com.telerikproject.controllers.rest;

import com.telerikproject.exceptions.PostDoesNotExistException;
import com.telerikproject.exceptions.UserDoesNotExistException;
import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import com.telerikproject.services.interfaces.PostService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private UserService userService;
    private PostService postService;

    @Autowired
    public UserRestController(UserService userService, PostService postService) {
        this.userService = userService;
        this.postService = postService;
    }

    @GetMapping("/isAuthorized")
    public Map<String, Boolean> isUserLogged(Principal principal) {
        if(principal == null) {
            return Collections.singletonMap("isLogged", false);
        }

        return Collections.singletonMap("isLogged", true);
    }

    @GetMapping("/currentUser")
    public User getCurrentUserId(Principal principal) {
        if(principal == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        try {
            return userService.getCurrentUser(principal);
        }
        catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/all")
    public List<User> getAllUsers() {
        try {
            return userService.getAllUsers();
        } catch (UserDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}/posts")
    public List<Post> getAllUserPostsById(@PathVariable int id) {
        try {
            return postService.getAllUserPostsById(id);
        } catch (PostDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable int id) {
        try {
            return userService.getUserById(id);
        } catch (UserDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/posts/{id}")
    public List<Post> getUserPosts(@PathVariable int id) {
        try {
            return userService.getUserPosts(id);
        } catch (PostDoesNotExistException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/delete/{username}")
    public void deleteUser(@PathVariable String username) {
        try {
            userService.deleteUser(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}