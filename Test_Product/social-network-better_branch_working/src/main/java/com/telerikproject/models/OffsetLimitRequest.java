package com.telerikproject.models;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class OffsetLimitRequest implements Pageable {

    private long offset;
    private int limit;

    public OffsetLimitRequest(long offset, int limit){
        this.limit = limit;
        this.offset = offset;
    }
    @Override
    public int getPageNumber() {
        return 0;
    }
    @Override
    public int getPageSize() {
        return limit;
    }
    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public Sort getSort() {
        throw new NotImplementedException();
    }

    @Override
    public Pageable next() {
        return new OffsetLimitRequest(offset + 1, 10);
    }

    @Override
    public Pageable previousOrFirst() {
        if(hasPrevious())
            return new OffsetLimitRequest(offset - 1, 10);
        else
            return new OffsetLimitRequest(1, 10);
    }

    @Override
    public Pageable first() {
        return new OffsetLimitRequest(1, 10);
    }

    @Override
    public boolean hasPrevious() {
        return offset - 1 > 0;
    }
}
