package com.telerikproject.services.interfaces;

import com.telerikproject.DTOs.PostDTO;
import com.telerikproject.models.Post;
import com.telerikproject.models.User;

import java.security.Principal;
import java.util.List;

public interface PostService {

    Post createPost(PostDTO postDTO, User user);

    Post getPostById(Integer id);

    List<Post> getAll();

    List<Post> getAllCurrentUserPosts(User user);

    List<Post> getAllPublicPostsOrderByPostDateDesc();

    List<Post> getAllUserPostsById(Integer id);

    Post editPost(Integer id, Post post);

    void deletePost(Integer id);
}
