package com.telerikproject.services;

import com.telerikproject.models.Like;
import com.telerikproject.models.Post;
import com.telerikproject.models.User;
import com.telerikproject.repositories.LikeRepository;
import com.telerikproject.repositories.PostRepository;
import com.telerikproject.services.interfaces.LikeService;
import com.telerikproject.services.interfaces.PostService;
import com.telerikproject.services.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class LikeServiceImpl implements LikeService {

    private LikeRepository likeRepository;
    private PostService postService;
    private UserService userService;

    @Autowired
    public LikeServiceImpl(LikeRepository likeRepository, PostService postService, UserService userService) {
        this.likeRepository = likeRepository;
        this.postService = postService;
        this.userService = userService;
    }

    @Override
    public List<Like> getAllLikes() {
        List<Like> likes = likeRepository.findAll();

        if (likes.isEmpty()) {
            throw new EntityNotFoundException();
        }

        return likes;
    }

    @Override
    public List<Like> getAllLikesByPostId(Integer id) {
        return likeRepository.getAllByPostId(id);
    }

    @Override
    public Like getLikeById(Integer id) {
        Like like = likeRepository.getLikeByIdAndIsLikedTrue(id);

        if (like == null) {
            throw new EntityNotFoundException();
        }

        return like;
    }

    @Override
    public void deleteLikeById(Integer id) {
        Like like = likeRepository.getLikeByIdAndIsLikedTrue(id);

        if (like == null) {
            throw new EntityNotFoundException();
        }

        like.setLiked(false);

        likeRepository.saveAndFlush(like);
    }

    @Override
    public void likeAndDislikePost(Integer postId, User user) {
        Post post = postService.getPostById(postId);
        Optional<Like> likeOptional = likeRepository.getOptionalByUserAndPost(user, post);

        if(!likeOptional.isPresent()) {
            Like like = new Like(user, post, true);
            likeRepository.saveAndFlush(like);
        } else {
            Like like = likeRepository.getLikeByUserAndPost(user, post);
            likeRepository.delete(like);
            likeRepository.delete(likeOptional.get());
        }
    }
}