package com.telerikproject.services;

import com.telerikproject.models.Authority;
import com.telerikproject.repositories.AuthorityRepository;
import com.telerikproject.services.interfaces.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class AuthorityServiceImpl implements AuthorityService {

    private final AuthorityRepository authorityRepository;

    @Autowired
    public AuthorityServiceImpl(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }

    @Override
    public Authority getAuthorityByUsername(String username) {
        Optional<Authority> authority = authorityRepository.findById(username);

        if(!authority.isPresent()) {
            throw new EntityNotFoundException();
        }

        return authority.get();
    }

    @Override
    public void addNewAuthority(Authority newOne) {
        authorityRepository.save(newOne);
    }

    @Override
    public boolean doesAuthorityExist(String username) {
        return authorityRepository.existsById(username);
    }

    @Transactional
    @Override
    public void deleteAuthority(String username) {
        authorityRepository.deleteById(username);
    }
}
