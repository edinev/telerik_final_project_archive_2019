package com.telerikproject.services.interfaces;

import com.telerikproject.models.Authority;

public interface AuthorityService {

    Authority getAuthorityByUsername(String username);

    void addNewAuthority(Authority newOne);

    boolean doesAuthorityExist(String username);

    void deleteAuthority(String username);
}
