package com.telerikproject.services.interfaces;


import com.telerikproject.models.Connection;
import com.telerikproject.models.User;

import java.security.Principal;
import java.util.List;

public interface ConnectionService {

    List<User> getAllFriends(User user);

    List<Connection> getAllSendRequests(User user);

    void sendFriendRequest(String name, User user);

    void declineFriendRequest(String name, User user);

    void deleteFriendRequest(String name, User user);

    User acceptFriendRequest(String name, User user);

    List<User> getAllRequestsFromUsers();

    List<User> getAllFriendsByUser(User user);

}
