***<center> ***Project: Social Network***</center>***
<br>
<br>
Authors: <br>
    - Tsvetan Bratanov<br>
    - Emil Dinev<br>

---
***TABLE OF CONTENTS***

**01. Introduction**

**02. Objective**
 
**03. Scope**

**04. Testing Strategy**

**05. Features to be tested**

**06. Timeline**

**07. Testing Environments**

**08. Risks**

**09. Tools Used**

**10. Exit Criteria**

---

**1.	INTRODUCTION**
<br/>
 A 'Social Network' is a web application which is accessible by the public and its purpose is to connect people, create accounts, comment and lit posts, get a feed of the newest and most  relevant posts of connections. The goal of our network is to achieve this and perfect the already well known formula, our social network although inspide of 'Facebook' aims to better it and have its focus on the feed generation targeted to travelers and have its posts in the format of reviews or be a place to keep in touch with fellow students from Telerik Academy Java Cohort, May 2019.

---

**2. OBJECTIVE**
<br/>
The objective of this test plan is to serve as a guiding tool for our testing process. In order for us to ensure that the corret procedure is followed and most efficient tools are used for our testing. This test plan will outline our current timelines so we can track our progress and ensure no delays to the project timelines occur. 

---

**3.  SCOPE**
<br/>
The scope of this testing is to assess the working order for all functionalities to be included in the Social Network. In order for this task to be succeful we will need to do exploratory testing, manual testing, REST testing of the middle-ware services, BDD testing and Automated testing. 

---
**4. Testing Strategy**
<br/>
Our testing strategy for this project is to start by creating REST test cases to ensure the service portion of our system is working. Once the UI portion of the Social Network is working and in a usable state we will start by doing exploratory testing to ensure the system is in functional state. Once completed, several high level test cases will be created and prioritized. Once all high level test cases are executed, the highest priority test cases (starting by happy paths, extending to corner cases) will be setup as full system cases to be executed by us or independent testers. Once all high priority test cases are executed and are passing, we will automate them using an automation tool to be executed as a regression testing. 

- 4.1 Services Test Cases
The first portion of our testing will focus on automated test cases of the REST API of our application. We will create REST test cases assessing the functioning state of the Services of the application  via PostMan.

- 4.2 Exploratory Testing
Once we have a stable application with UI which can be utilized, we will start performing Exploratory Testing where we will go through all happy paths and most corner cases to ensure the web application is working as expected prior to completing any structured/scripted test cases. 

- 4.3 High Level Test Cases
Once we've completed our exploratory testing, high level test cases will be built to cover all 'happy paths' and several corner cases which are identified to have higher impact on the system functionality. Once test cases are drafted they will be prioritized for completion. 

- 4.4 Behavior Driven Development Test Cases
To ensure we are following the best Behavior Driven Development practices, a framework for our application will be built and several of the high priority test cases will be drafted within the framework to ensure further continuous integration if allowed for our application.

- 4.5 Manual System Test Cases
To have a concrete evidence that our application is working as expected, all high level test cases with high priority will be drafted into full scripted test cases. The test cases can be executed either by the QA staff or if available - by independent testers to have the most objective view of the assessment. 

- 4.6 Automation Test Cases
Once we have executed a high percentage of our system test cases with them passing we will automate the test cases following the 'happy paths' of our application. This will be done both for fast testing results for any small implementation of new functionality and for regression testing if any bigger releases are introduced.
---

**5. Features to be tested**
<br/>
Below are displayed the main functionalities that will be introduced as part of our 'Social Network' application and we are planning to test. 
The functionalities are separated in three categories, depending on the level of access to the features.

*1. Public Access (Unauthorized access - no account required)*<br/>
1.1. Application Home Page (With the various access points such as login, registration forms and profile search)<br/>
1.2 Login form<br/>
1.3 Registration form<br/>
1.4 Public Profiles<br/>
1.5 Search Profiles<br/>
1.6 Public Feed<br/>

*2. Private Access (Authorized users with un-elevated access)*<br/>
2.1 Profile administration<br/>
2.1.1 Change Name<br/>
2.1.2 Upload a profile picture<br/>
2.1.3 Set visibility of picture<br/>
2.1.4 Change password<br/>
2.1.5 Change email<br/>
<br/>
2.2 Post Creation<br/>
<br/>
2.3 Post interaction<br/>
2.3.1 Like a post<br/>
2.3.2 Comment a post<br/>
<br/>

*3. Administration Portion (Users with elevated access)*<br/>
3.1 Edit/Delete Profiles<br/>
3.2 Edit/Delete posts<br/>
3.3 Edit/Delete comments<br/>
<br/>

---

**6. Timeline**
<br/>
On the table displayed below, we've drafted the timeline which be followed during out testing process. 
The timeline will allow us to track our progress and any possible delays. 

| Task                              | Time Required |
|-----------------------------------|---------------|
| Create Test Plan                  | 2 Days        |
| Create REST API Test Cases        | 5 Days        |
| Create High Level Test Cases      | 2 Days        |
| Create System Test Cases          | 4 Days        |
| Create BDD Framework and Scripts  | 4 Days        |
| Create Automation Framework       | 5 Days        |
| Automate High Priority Test Cases | 2 Days        |
| Generate Testing Report           | 2 Days        |
| Create Presentation               | 2 Days        |

---

**7. Testing Environments**
<br/>
The testing process will be executed on the QA team's machines directly. As our Social Network application will not be hosted on a live server,
we will perform all test executions on the following machines and while our machines lack severly different Operating System capabilities, our extensive testing
should prevent any type of incidents being missed. 

| System Specification   | QA Machine 1              | QA Machine 2              |
|------------------------|---------------------------|---------------------------|
| Operating System       | Microsoft Windows 10 Home | Microsoft Windows 10 Home |
| Software Specification |                           |                           |
| Internet Explorer      | v11                       | v11                       |
| Chrome                 | 78.0.3904.108             | 78.0.3904.97              |
| Firefox                | 60.9.0                    | 70.0.1                    |
| Edge                   | 41.16299.1419.0           | 44.18362.387.0            |

---

**8. Risks**
<br/>
During development and testing of our software various issues might occur, this section of the test plan would try to best analyze what kind of issues
we might encounter and how to best mitigate them.  

| Risk                    | Risk Type    | Description                                                                                                                         | Impact | Mitigation                                                                                                                                                                      |
|-------------------------|--------------|-------------------------------------------------------------------------------------------------------------------------------------|--------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Defect Prone Software   | Product Risk | The software provided contains more bugs due to the lack of experience of the developers and the fairly vague design specification. | High   | Extensive communication between the QA, Developers and Telerik Mentors.                                                                                                         |
| Developer Experience    | Project Risk | Due to relative inexperience of the developers  communication will be lesser than expected.                                         | Medium | Spending time explaining the role of the QA and what is required from the developers for our project to be properly executed.                                                   |
| QA Engineers Experience | Project Risk | Due to relative inexperience of the QA engineers the testing of the product might be lesser than expected.                          | Medium | Utilizing various procedures and going through already provided documentation to ensure the testing is extensive enough to catch a majority of the potential software failures. |

---

***9. Tools Used***
<br/>
On the table below are the tools used during our QA testing process and their purpose. 

| Tool               | Tool Type                 | Purpose                                                                                                   |
|--------------------|---------------------------|-----------------------------------------------------------------------------------------------------------|
| Trello             | Task Management Software  | To track the progress and manage the QA testing tasks.                                                    |
| GitLab             | SCM software              | To create a QA repository and gain access to the Social Network source code.                              |
| InteliJ            | IDE                       | To access, create and run the Social Network application  and to incorporate with the Selenium Webdriver. |
| MariaDB            | Database Management       | To create the database for the project.                                                                   |
| PostMan            | Service Testing Software  | To create automation test cases assessing the API of the project.                                         |
| JBehave            | Framework                 | A framework used for BDD testing.                                                                         |
| Selenium WebDriver | Automated Web Testing     | Used to create automated test cases to assess the UI of the social network application.                   |
| Microsoft Office   | Document editing software | To create and modify the documentation backing our process, including the Test Plan.                      |

---
***10. Exit Criteria***
<br/>
This section will be used to track our progress and to know when to stop testing. The most basic principle will be listed below, we will stop testing when the following
criteria are met:
- 100% of the High Priority Manual test cases are executed.
- 100% of the High Priority Manual test cases are automated and executed.
- 90% of the discovered bugs (defects) or failures are logged accordingly.
- 70% of the lower priority manual test cases are executed.

This is very vague and short exit criteria mainly due to the factor that we do not have a constant developer support and our project will not receive further adjustments
the more we approach the 'go-live date'. 