package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.security.Key;
import java.util.concurrent.TimeUnit;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.AWTException;

import static junit.framework.TestCase.assertTrue;

public class UserActions {
	final WebDriver driver;

	public UserActions() {
		this.driver = Utils.getWebDriver();
	}

	public static void loadBrowser() {
		Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
	}

	public static void quitDriver(){
		Utils.tearDownWebDriver();
	}

	public void clickElement(String key){
		waitForElementVisible(key, 10);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		element.click();
		Utils.LOG.info("Element " + key + " is clicked.");

	}

	public void typeValueInField(String value, String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.sendKeys(value);
		Utils.LOG.info("Value :" + value + " is typed in " + field);
	}

	//############# WAITS #########

	public void waitForElementVisible(String locator, int seconds){
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
		Utils.LOG.info("Element " + locator + " is waited for " + seconds + " sec. and it is visible.");
	}

	public void waitForElementClickable(String locator, int seconds) {
		WebDriverWait wait= new WebDriverWait(driver,seconds);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(locator))));
		Utils.LOG.info("Element " + locator + " is waited for " + seconds + " sec. and now it is clickable.");
	}

	//############# ASSERTS #########

	public void assertElementPresent(String locator){
		Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
		Utils.LOG.info("Element " + locator + " is present.");
	}

	public void assertElementNotPresent(String locator) {
		Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
	}

	public void assertElementContains (String locator, String expectedResult){
		String actualString = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getText();
		String expectedString = expectedResult;
		assertTrue(actualString.contains(expectedString));
		Utils.LOG.info("Element " + locator + " contains " + expectedResult);
	}
	public void inputNewTopicTitle(String title){
		waitForElementVisible("xpath.createTopicTitleField",10);
		typeValueInField(title,"xpath.createTopicTitleField");
	}

	public void inputNewTopicMainText(String text){
		waitForElementVisible("xpath.createTopicTextArea",10);
		typeValueInField(text,"xpath.createTopicTextArea");
	}

	public void clearElementOfInput (String field){
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.clear();
		Utils.LOG.info("Text in element " + field + " is cleared");

	}

	public void clickElementTag(String xpath) {
		waitForElementClickable(xpath, 2);
		clickElement(xpath);
		clickElement(xpath);
	}

	public void clickPseudoElement() {
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath("/html/body/section/div/div/div[1]/div[1]/div[1]/a/i")))
				.click().build().perform();
	}

	public void uploadProfilePicture() {
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath("/html/body/section/div/div/div[1]/div[1]/div[1]/div[1]/form/input[1]")))
				.click().build().perform();

		driver.switchTo().activeElement().sendKeys("C:\\c.jpg");

		try
		{
			Robot robot = new Robot();

			// press C key
			robot.keyPress(KeyEvent.VK_C);
			robot.keyRelease(KeyEvent.VK_C);

			// press SHIFT+SEMICOLON key
			robot.keyPress(KeyEvent.VK_SHIFT);
			robot.keyPress(KeyEvent.VK_SEMICOLON);
			robot.keyRelease(KeyEvent.VK_SEMICOLON);
			robot.keyRelease(KeyEvent.VK_SHIFT);

			// press BACKSLASH key
			robot.keyPress(KeyEvent.VK_BACK_SLASH);
			robot.keyRelease(KeyEvent.VK_BACK_SLASH);

			// press C key
			robot.keyPress(KeyEvent.VK_C);
			robot.keyRelease(KeyEvent.VK_C);

			// press DOT key
			robot.keyPress(0x2e);
			robot.keyRelease(0x2e);

			// press J key
			robot.keyPress(KeyEvent.VK_J);
			robot.keyRelease(KeyEvent.VK_J);

			// press P key
			robot.keyPress(KeyEvent.VK_P);
			robot.keyRelease(KeyEvent.VK_P);

			// press G key
			robot.keyPress(KeyEvent.VK_G);
			robot.keyRelease(KeyEvent.VK_G);

			// press TAB key twice
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);

			// click ENTER key
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			action.sendKeys(Keys.ENTER);

		}
		catch (AWTException e)
		{
			e.printStackTrace();
		}
	}

	public void enterNewURL(String url) {
		driver.get(url);
		Utils.LOG.info("entered URL:" + Utils.getConfigPropertyByKey("base.url"));
	}

}
