package testCases;

import com.telerikacademy.testframework.Utils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class socialNetworkTestCases extends BaseTest {

    @Test
    public void TEST_01_Profile_Creation_Login_Form() {

//        C173 PROFILE CREATION/LOGIN FORM
//        PRECONDITIONS
//        No test specific preconditions are applicable to this test case.
//        The test collection prerequisites are required to be able to complete the test.


//        Step 1.
//        Access the 'Social Network' application.
        Utils.LOG.info(
                "\n\n--------------------------------------------------------\n" +
                "-----RUN TEST CASE C173 PROFILE CREATION/LOGIN FORM-----\n" +
                "--------------------------------------------------------");
        Utils.LOG.info("\n\n-----Test Case C173 - Step 1-----");
        actions.enterNewURL("http://localhost:8080/");


//        Step 2.
//        Verify the following items are present on the 'home' page:
//        - Link to a registration page
//        - Link to a login page
//        - A Search bar (profile search)
        Utils.LOG.info("\n\n-----Test Case C173 - Step 2-----");

        actions.assertElementPresent("xpath.SignUpLink");
        actions.assertElementPresent("xpath.LoginLink");
        actions.assertElementPresent("xpath.MembersLink");


//        Step 3.
//        Press on the Registration form link.
        Utils.LOG.info("\n\n-----Test Case C173 - Step 3-----");
        actions.clickElement("xpath.SignUpLink");


//        Step 4.
//        Verify the following fields are present in the form:
//        - Gender radio buttons.
//        - First name input field.
//        - Last name input field.
//        - Email input field.
//        - Password input field.
//        - Confirm password input field.
        Utils.LOG.info("\n\n-----Test Case C173 - Step 4-----");
        actions.waitForElementClickable("xpath.RegisterFirstNameField", 2);
        actions.assertElementPresent("xpath.RegisterMaleRadioButton");
        actions.assertElementPresent("xpath.RegisterFemaleRadioButton");
        actions.assertElementPresent("xpath.RegisterFirstNameField");
        actions.assertElementPresent("xpath.RegisterLastNameField");
        actions.assertElementPresent("xpath.RegisterEmailField");
        actions.assertElementPresent("xpath.RegisterPasswordField");
        actions.assertElementPresent("xpath.ConfirmPasswordField");
        actions.assertElementPresent("xpath.RegisterSubmitButton");


//        Step 5.
//        Create an account, enter valid information for all fields.
        Utils.LOG.info("\n\n-----Test Case C173 - Step 5-----");
        actions.clickElement("xpath.RegisterFirstNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("firstNameRegularUser"),"xpath.RegisterFirstNameField");
        actions.clickElement("xpath.RegisterLastNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("lastNameRegularUser"),"xpath.RegisterLastNameField");
        actions.clickElement("xpath.RegisterEmailField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("usernameRegularUser"), "xpath.RegisterEmailField");
        actions.clickElement("xpath.RegisterPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("passwordRegularUser"),"xpath.RegisterPasswordField");
        actions.clickElement("xpath.ConfirmPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("passwordRegularUser"), "xpath.ConfirmPasswordField");
        actions.clickElement("xpath.RegisterMaleRadioButton");
        actions.clickElement("xpath.RegisterSubmitButton");


//        Step 6.
//        Verify a confirmation message is displayed that the user is created.
        Utils.LOG.info("\n\n-----Test Case C173 - Step 6-----");


//        Step 7.
//        Access the application home-page.
        Utils.LOG.info("\n\n-----Test Case C173 - Step 7-----");
        actions.enterNewURL("http://localhost:8080/");


//        Step 8.
//        Access the 'Login form'.
        Utils.LOG.info("\n\n-----Test Case C173 - Step 8-----");
        actions.waitForElementClickable("xpath.LoginLink", 2);
        actions.clickElement("xpath.LoginLink");


//        Step 9.
//        Verify the following fields are present:
//        - Username (or email address)
//        - Password
        Utils.LOG.info("\n\n-----Test Case C173 - Step 9-----");
        actions.waitForElementClickable("xpath.LoginButtonLoginForm", 2);
        actions.assertElementPresent("xpath.LoginFormEmailAddressField");
        actions.assertElementPresent("xpath.LoginFormPasswordField");


//        Step 10.
//        Login using the details setup in step 5
        Utils.LOG.info("\n\n-----Test Case C173 - Step 10-----");
        actions.clickElement("xpath.LoginFormEmailAddressField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("usernameRegularUser"), "xpath.LoginFormEmailAddressField");
        actions.clickElement("xpath.LoginFormPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("passwordRegularUser"), "xpath.LoginFormPasswordField");
        actions.clickElement("xpath.LoginButtonLoginForm");
        actions.waitForElementClickable("xpath.LogoutButton", 2);
        actions.assertElementPresent("xpath.PersonalProfileButton");

        actions.clickElement("xpath.LogoutLink");
        actions.enterNewURL("http://localhost:8080/");


//        EXPECTED RESULTS
//        The tester is able to access the application home-page and verify the login form,
//        registration form links and profile search bar are present (step2).
//        Upon clicking on the registration form, the tester will be able to access the form
//        and verify that all required fields for an account creation are present and required (step 4).
//        The tester is able to create an account via the form and login using the account afterwards (step 10).

    }

    @Test
    public void TEST_02_Elevated_Access_Modify_Comment_Post_User_Account() {

//        C318 ELEVATED ACCESS - MODIFY A COMMENT, POST AND USER ACCOUNT
//        PRECONDITIONS
//        1. This test requires elevated access to be created and utilized,
//           the account will be referred as elevated below.
//        2. Create a regular account within the application, the account
//        will be referred below as primary.


//        Step 1.
//        Access the application, log in with the primary account.
        Utils.LOG.info(
                "\n\n---------------------------------------------------------------------------------------\n" +
                        "-----RUN TEST CASE C318 ELEVATED ACCESS - MODIFY A COMMENT, POST AND USER ACCOUNT-----\n" +
                        "--------------------------------------------------------------------------------------");
        Utils.LOG.info("\n\n-----Test Case C318 - Step 1-----");

        loginRegularUser();


//        Step 2.
//        Create a post with a description only, set to public.
        Utils.LOG.info("\n\n-----Test Case C318 - Step 2-----");
        actions.clickElement("xpath.WallPostTextArea");
        actions.typeValueInField("Regular user creates a post. Test Case C319",
                "xpath.WallPostTextArea");

        actions.clickElement("xpath.WallPostSubmitButton");


//        Step 3.
//        Comment on the post.
        actions.clickElement("xpath.WallCommentTextArea");
        actions.typeValueInField("Regular user comments the post. Test Case C319",
                "xpath.WallCommentTextArea");
        actions.clickElement("xpath.WallAddCommentButton");


//        Step 4.
//        Log out of the application.
        actions.clickElement("xpath.LogoutButton");
        actions.enterNewURL("http://localhost:8080/");


//        Step 5.
//        Log in the application with the elevated account.
        loginAdminUser();


//        Step 6.
//        Search for the primary account, find the post created in step 2.
        actions.waitForElementClickable("xpath.NavBarAdminPanel", 2);
        actions.assertElementPresent("xpath.NavBarAdminPanel");
        actions.clickElement("xpath.NavBarAdminPanel");


//        Step 7.
//        Modify the comment on the post (description).
        actions.waitForElementClickable("xpath.CommentTableUpdateCommentButton",2);
        actions.clickElement("xpath.CommentTableUpdateCommentButton");
        clearInputElement("xpath.CommentTableEditCommentElement");
        actions.typeValueInField("Edited by Admin C319.","xpath.CommentTableEditCommentElement");
        actions.clickElement("xpath.CommentTableEditSubmitButton");


//        Step 8.
//        Modify the post (description).
        actions.waitForElementClickable("xpath.PostsTablesEditPostButton",2);
        actions.clickElement("xpath.PostsTablesEditPostButton");
        clearInputElement("xpath.PostsTableEditCommentElement");
        actions.typeValueInField("Edited by Admin C319.","xpath.PostsTableEditCommentElement");
        actions.clickElement("xpath.PostsTableEditSubmitButton");


//        Step 9.
//        Modify the primary account (change the first name for ex).
        actions.waitForElementClickable("xpath.UserTableEditUserButton", 2);
        actions.clickElement("xpath.UserTableEditUserButton");
        clearInputElement("xpath.UserTableFistNameUserElement");
        actions.typeValueInField("User","xpath.UserTableFistNameUserElement");
        actions.clickElement("xpath.UserTableEditSubmitButton");


//        Step 10.
//        Close out the application.
        actions.clickElement("xpathAdminEditPanelBackToHomePageLink");
        actions.clickElement("xpath.LogoutLink");



//        EXPECTED RESULTS
//        The tester is able to modify with elevated access:
//            1. the comment (step 7)
//            2. the post (step 8)
//            3. the user account (step 9)
    }

    @Test
    public void TEST_03_Public_Feed_and_profiles_Profile_Search() {

//        C174 PUBLIC FEED AND PROFILES/PROFILE SEARCH
//        PRECONDITIONS
//        The following accounts must be created prior to test execution.
//        - A profile must be created with all attributed set to public, including posts.
//        The account will be referred to as 'primary' below.
//        - The account 'primary' must have at least two posts generated.


//        Step 1.
//        Access the Social Network application.
        actions.enterNewURL("http://localhost:8080/");
        loginAdminUser();


//        Step 2.
//        Verify that the posts of the primary account are visible in the 'public feed'.
        actions.waitForElementVisible("xpath.HomePageMemberAvatarOnTheWall",2);
        actions.assertElementPresent("xpath.HomePageMemberAvatarOnTheWall");


//        Step 3.
//        Use the 'Profile Search' to search for the primary account.
        actions.clickElement("xpath.MembersLink");
        actions.waitForElementClickable("xpath.MembersPanelSearch", 2);
        actions.clickElement("xpath.MembersPanelSearch");
        actions.typeValueInField("User Regular", "xpath.MembersPanelSearch");


//        Step 4.
//        Access the account, verify the following:
//        - The profile name is visible
//        - Profile posts are visible for the account
        actions.waitForElementClickable("xpath.MembersPanelViewProfileButton", 2);
        actions.clickElement("xpath.MembersPanelViewProfileButton");
        actions.assertElementContains("xpath.ProfilePanelNameField","User");
        actions.waitForElementClickable("xpath.ProfilePanelLikeButton",2);
        actions.assertElementPresent("xpath.ProfilePanelWallBody");


//        The tester is able to access the application and verify that posts set to public are visible through
//        the 'public feed' of the application chronological order (step 2). The tester is able to search the
//        profile listed in the preconditions of this test case using the 'Profile Search' functionality and
//        view all information set to public including the posts of the account (step 4).


    }

    @Test
    public void TEST_04_Elevated_Access_Delete_Comment_Post_and_User_Account() {

//        C181 ELEVATED ACCESS - DELETE A COMMENT, POST AND USER ACCOUNT
//
//        PRECONDITIONS
//        1. This test requires elevated access to be created and utilized, the account will be referred as elevated below.
//        2. Create a regular account within the application, the account will be referred below as primary.


//        Step 1.
//        Access the application, log in with the elevated account.
        actions.enterNewURL("http://localhost:8080/login");
        actions.waitForElementClickable("xpath.LoginFormEmailAddressField",2);
        actions.clickElement("xpath.LoginFormEmailAddressField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("usernameAdminUser"), "xpath.LoginFormEmailAddressField");
        actions.clickElement("xpath.LoginFormPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("passwordAdminUser"), "xpath.LoginFormPasswordField");
        actions.clickElement("xpath.LoginButtonLoginPage");
        actions.assertElementPresent("xpath.LogoutLink");


//        Step 2.
//        Search and access the primary account.
        actions.waitForElementClickable("xpath.NavBarAdminPanel", 2);
        actions.assertElementPresent("xpath.NavBarAdminPanel");
        actions.clickElement("xpath.NavBarAdminPanel");


//        Step 3.
//        Open the post, delete the comment created by the primary account.
        actions.waitForElementClickable("xpath.CommentTableDeleteCommentButton",2);
        actions.clickElement("xpath.CommentTableDeleteCommentButton");


//        Step 4.
//        Log out of the application.
        actions.clickElement("xpathAdminEditPanelBackToHomePageLink");
        actions.waitForElementClickable("xpath.LogoutButton", 2);
        actions.clickElement("xpath.LogoutButton");


//        Step 5.
//        Search and access the primary account.
        actions.enterNewURL("http://localhost:8080/");


//        Step 6.
//        Verify that the post is visible and the comment is not.
        //actions.assertElementNotPresent("//div[contains(text(),'Regular user comments post')]");


        // Step 7.
        // Log into the application with the elevated account.
        actions.clickElement("xpath.LoginLink");
        actions.waitForElementClickable("xpath.LoginFormEmailAddressField",2);
        actions.clickElement("xpath.LoginFormEmailAddressField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("usernameAdminUser"), "xpath.LoginFormEmailAddressField");
        actions.clickElement("xpath.LoginFormPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("passwordAdminUser"), "xpath.LoginFormPasswordField");
        actions.clickElement("xpath.LoginButtonLoginPage");
        //actions.assertElementPresent("xpath.LogoutButtonUserWall");


        // Step 8.
        // Search and access the primary account.
        actions.waitForElementClickable("xpath.NavBarAdminPanel", 2);
        actions.assertElementPresent("xpath.NavBarAdminPanel");
        actions.clickElement("xpath.NavBarAdminPanel");


        // Step 9.
        // Delete the post created by the primary account.
        actions.waitForElementClickable("xpath.PostsTablesDeletePostButton",2);
        actions.clickElement("xpath.PostsTablesDeletePostButton");


        // Step 10.
        // Log out of the application.
        actions.clickElement("xpathAdminEditPanelBackToHomePageLink");
        actions.waitForElementClickable("xpath.LogoutButton", 2);
        actions.clickElement("xpath.LogoutButton");


        // Step 11.
        // Search and access the primary account.
        actions.enterNewURL("http://localhost:8080/");


        // Step 12.
        // Verify the post created by the primary account is not visible.


        // Step 13.
        // Log into the application with the elevated account.
        actions.clickElement("xpath.LoginLink");
        actions.waitForElementClickable("xpath.LoginFormEmailAddressField",2);
        actions.clickElement("xpath.LoginFormEmailAddressField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("usernameAdminUser"), "xpath.LoginFormEmailAddressField");
        actions.clickElement("xpath.LoginFormPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("passwordAdminUser"), "xpath.LoginFormPasswordField");
        actions.clickElement("xpath.LoginButtonLoginPage");


        // Step 14.
        // Search and access the primary account.
        actions.waitForElementClickable("xpath.NavBarAdminPanel", 2);
        actions.assertElementPresent("xpath.NavBarAdminPanel");
        actions.clickElement("xpath.NavBarAdminPanel");


        // Step 15.
        // Delete the whole account.
        actions.waitForElementClickable("xpath.UsersTableDeleteUserButton",2);
        actions.clickElement("xpath.UsersTableDeleteUserButton");


        // Step 16.
        // Log out of the application.
        actions.clickElement("xpathAdminEditPanelBackToHomePageLink");
        actions.waitForElementClickable("xpath.LogoutButton", 2);
        actions.clickElement("xpath.LogoutButton");

        // Step 17.
        // Search for the primary account, verify the account is not present.

        // Step 18.
        // Close out the application.
        actions.enterNewURL("http://localhost:8080/");
    }

    @Test
    public void TEST_05_Profiles_Connection() {

//        C179 Profile Connection
//        PRECONDITIONS
//        Create an account within the application, the account will be referred through the test as primary.
//        Create a second account within the application, the account will be referred through the test as secondary.


//        PRECONDITION STEP
        actions.enterNewURL("http://localhost:8080/");
        actions.clickElement("xpath.SignUpLink");
        actions.clickElement("xpath.RegisterFirstNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("firstNameRegularUser"),"xpath.RegisterFirstNameField");
        actions.clickElement("xpath.RegisterLastNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("lastNameRegularUser"),"xpath.RegisterLastNameField");
        actions.clickElement("xpath.RegisterEmailField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("usernameRegularUser"), "xpath.RegisterEmailField");
        actions.clickElement("xpath.RegisterPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("passwordRegularUser"),"xpath.RegisterPasswordField");
        actions.clickElement("xpath.ConfirmPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("passwordRegularUser"), "xpath.ConfirmPasswordField");
        actions.clickElement("xpath.RegisterMaleRadioButton");
        actions.clickElement("xpath.RegisterSubmitButton");


//        Step 1.
//        Access the 'Social Network', log in with the primary account.
        actions.enterNewURL("http://localhost:8080/");
        loginRegularUser();


//        Step 2.
//        Search for the secondary account.
        actions.waitForElementClickable("xpath.MembersLink", 2);
        actions.clickElement("xpath.MembersLink");
        actions.waitForElementClickable("xpath.MembersPanelSearch", 2);
        actions.clickElement("xpath.MembersPanelSearch");
        actions.clearElementOfInput("xpath.MembersPanelSearch");
        actions.typeValueInField("Admin", "xpath.MembersPanelSearch");


//        Step 3.
//        Send a friend request or connection.
        actions.waitForElementClickable("xpath.MembersPanelAddFriendButton", 2);
        actions.clickElement("xpath.MembersPanelAddFriendButton");


//        Step 4.
//        Log out of the application.
        actions.enterNewURL("http://localhost:8080/");
        actions.waitForElementClickable("xpath.LogoutLink", 2);
        actions.clickElement("xpath.LogoutLink");
        actions.enterNewURL("http://localhost:8080/");


//        Step 5.
//        Log back in using the secondary account.
        loginAdminUser();


//        Step 6.
//        Verify the connection request and the following options for it are available:
//        - Accept
//        - Reject
        actions.waitForElementClickable("xpath.MembersLink", 2);
        actions.clickElement("xpath.MembersLink");
        actions.waitForElementClickable("xpath.MembersPanelAcceptRequestButton", 2);
        actions.assertElementPresent("xpath.MembersPanelDeclineRequestButton");
        actions.assertElementPresent("xpath.MembersPanelAcceptRequestButton");


//        Step 7.
//        Accept the request.
        actions.clickElement("xpath.MembersPanelAcceptRequestButton");

//        Step 8 - 9
//        Search for the primary account.
//        Access the account and verify that both accounts are connected.
        actions.waitForElementClickable("xpath.MembersPanelViewProfileButton", 2);
        actions.clickElement("xpath.MembersPanelViewProfileButton");


//        Step 10 - 14.
//        Reject the connection request from the secondary account.
        actions.waitForElementClickable("xpath.MembersLink", 2);
        actions.clickElement("xpath.MembersLink");
        actions.waitForElementClickable("xpath.MembersPanelDeclineRequestButton", 2);
        actions.clickElement("xpath.MembersPanelDeclineRequestButton");


//        Logout
        actions.enterNewURL("http://localhost:8080/");
        actions.waitForElementClickable("xpath.LogoutLink", 2);
        actions.clickElement("xpath.LogoutLink");
        actions.enterNewURL("http://localhost:8080/");


//        This test case fails due to 'Accept Friend Request' and 'Decline Friend Request'
//        features of our 'Social Network' application not working.
    }

    @Test
    public void TEST_06_Login_Form_Boundaries() {
        //Corner case to assure the system does not accept incorrect credentials, for already
        //existing accounts.


        actions.enterNewURL("http://localhost:8080/");
        actions.waitForElementClickable("xpath.LoginLink", 2);
        actions.clickElement("xpath.LoginLink");
        actions.waitForElementClickable("xpath.LoginFormEmailAddressField", 2);
        actions.clickElement("xpath.LoginFormEmailAddressField");
        actions.typeValueInField("WrongUsername@mail.com", "xpath.LoginFormEmailAddressField");
        actions.clickElement("xpath.LoginFormPasswordField");
        actions.typeValueInField("WrongPassword", "xpath.LoginFormPasswordField");
        actions.clickElement("xpath.LoginButtonLoginForm");
        actions.assertElementContains("xpath.WrongCredentialsError", "Wrong username or password.");
        //above steps cover accessing the system with non-existing username and password
        actions.clickElement("xpath.LoginFormEmailAddressField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("usernameRegularUser"), "xpath.LoginFormEmailAddressField");
        actions.clickElement("xpath.LoginFormPasswordField");
        actions.typeValueInField("WrongPassword", "xpath.LoginFormPasswordField");
        actions.clickElement("xpath.LoginButtonLoginForm");
        actions.assertElementContains("xpath.WrongCredentialsError", "Wrong username or password.");
        //above steps cover accessing the system with existing correct username and incorrect password
        actions.clickElement("xpath.LoginFormEmailAddressField");
        actions.typeValueInField("WrongUsername@mail.com", "xpath.LoginFormEmailAddressField");
        actions.clickElement("xpath.LoginFormPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("passwordRegularUser"), "xpath.LoginFormPasswordField");
        actions.clickElement("xpath.LoginButtonLoginForm");
        actions.assertElementContains("xpath.WrongCredentialsError", "Wrong username or password.");
        //above steps cover accessing the system with incorrect username and correct password assigned to an
        //existing account.
        actions.clickElement("xpath.LoginFormEmailAddressField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("usernameRegularUser"), "xpath.LoginFormEmailAddressField");
        actions.clickElement("xpath.LoginButtonLoginForm");
        actions.assertElementContains("xpath.WrongCredentialsError", "Wrong username or password.");

        //above steps cover accessing the system with correct username and no password.

        actions.clickElement("xpath.LoginFormPasswordField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("passwordRegularUser"), "xpath.LoginFormPasswordField");
        actions.clickElement("xpath.LoginButtonLoginForm");
        actions.assertElementContains("xpath.WrongCredentialsError", "Wrong username or password.");

        //above steps cover accessing the system with correct password and no username.

        actions.clickElement("xpath.LoginButtonLoginForm");
        actions.assertElementContains("xpath.WrongCredentialsError", "Wrong username or password.");

        //above sptes cover attempting to access the system with no credentials entered.

    }

    @Test
    public void TEST_07_Administration_Page() {

        String firstNameC177 = "User";
        String lastNameC177 = "Regular";
        String cityC177 = "Sofia_C177";
        String countryC177 = "Bulgaria_C177";
        String aboutC177 = "Neque porro quisquam C177";

//        C177 ADMINISTRATION PAGE
//        PRECONDITIONS
//        1. An account must be created within the system, the account will be referred to as 'primary' below.
//        2. A picture abiding to size and format restrictions must be ready to be used during testing.


//        Step 1.
//        Navigate to Login pageAccess the 'Social Network' application, login using the primary account credentials.
        actions.enterNewURL("http://localhost:8080/");
        loginRegularUser();


//        Step 2.
//        Access the 'Profile Administration' page.
        actions.waitForElementClickable("xpath.NavBarProfile", 2);
        actions.assertElementPresent("xpath.NavBarProfile");
        actions.clickElementTag("xpath.NavBarProfile");
        actions.assertElementPresent("xpath.UserSettingsPageChangeSettingButton");


//        Step 3.
//        Change the first name and last name of the account.
        actions.waitForElementClickable("xpath.UserSettingsPageChangeSettingButton", 2);
        actions.clickElement("xpath.UserSettingsPageChangeSettingButton");
        actions.clickPseudoElement();
        actions.waitForElementClickable("xpath.UserSettingsPopupWindowFirstName", 2);
        actions.clickElement("xpath.UserSettingsPopupWindowFirstName");
        actions.clearElementOfInput("xpath.UserSettingsPopupWindowFirstName");
        actions.typeValueInField(firstNameC177,"xpath.UserSettingsPopupWindowFirstName");
        actions.clickElement("xpath.UserSettingsPopupWindowLastName");
        actions.clearElementOfInput("xpath.UserSettingsPopupWindowLastName");
        actions.typeValueInField(lastNameC177,"xpath.UserSettingsPopupWindowLastName");


//        Step 4.
//        Change city, country and about fields of the account.
        actions.clickElement("xpath.UserSettingsPopupWindowCity");
        actions.clearElementOfInput("xpath.UserSettingsPopupWindowCity");
        actions.typeValueInField(cityC177, "xpath.UserSettingsPopupWindowCity");
        actions.clickElement("xpath.UserSettingsPopupWindowCountry");
        actions.clearElementOfInput("xpath.UserSettingsPopupWindowCountry");
        actions.typeValueInField(countryC177, "xpath.UserSettingsPopupWindowCountry");
        actions.clickElement("xpath.UserSettingsPopupWindowAbout");
        actions.clearElementOfInput("xpath.UserSettingsPopupWindowAbout");
        actions.typeValueInField(aboutC177, "xpath.UserSettingsPopupWindowAbout");


//        Step 5.
//        Save the changes.
        actions.clickElement("xpath.UserSettingsPopupWindowSaveButton");
        actions.assertElementContains("xpath.UserSettingsMainPageName",firstNameC177);


//        Step 6.
//        Upload a profile picture.
        actions.uploadProfilePicture();
        actions.waitForElementClickable("xpath.UserSettingsUploadFileButton",2);
        actions.clickElement("xpath.UserSettingsUploadFileButton");


//        Step 7.
//        Change the password of the account (optional).


//        Step 8.
//        Change the email of the account (optional).


//        Step 9.
//        Close the application.
        actions.clickElement("xpath.LogoutLink");
        actions.enterNewURL("http://localhost:8080/");


//        EXPECTED RESULT
//        The user is able to access with the account generated as the per the preconditions of the test,is able to:
//        - access the Profile Administration page (step 2).
//        - change his name (step 3)
//        - change his About info (step 4)
//        - The tester is also able to upload a profile picture (step 6).
//        ACTUAL RESULTS
//        The function Upload Profile Picture does not work correctly. User can choose a picture
//        but when is uploaded it is not visible.
    }

}
