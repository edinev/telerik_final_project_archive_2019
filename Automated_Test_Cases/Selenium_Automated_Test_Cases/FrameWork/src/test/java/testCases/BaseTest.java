package testCases;

import com.telerikacademy.testframework.CustomWebDriverManager;
import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public class BaseTest {
	UserActions actions = new UserActions();
	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser();

	}

	@AfterClass
	public static  void tearDown(){
	UserActions.quitDriver();
	}

//	String usernameAdminUser = "test01@abv.bg";
//	String passwordAdminUser = "Qwerty1";
//
//	String firstNameRegularUser = "User";
//	String lastNameRegularUser = "Regular";
//	String usernameRegularUser = "user_regular@abv.bg";
//	String passwordRegularUser = "Qwerty1";


	public void clearInputElement(String xpath) {
		actions.waitForElementClickable(xpath,2);
		actions.clickElement(xpath);
		actions.clearElementOfInput(xpath);
		Utils.LOG.info(xpath + " element is cleared");
	}


	public void loginRegularUser() {
		actions.waitForElementClickable("xpath.LoginLink",2);
		actions.clickElement("xpath.LoginLink");
		actions.assertElementPresent("xpath.LoginLogo");
		actions.clickElement("xpath.LoginFormEmailAddressField");
		actions.typeValueInField(Utils.getConfigPropertyByKey("usernameRegularUser"), "xpath.LoginFormEmailAddressField");
		actions.clickElement("xpath.LoginFormPasswordField");
		actions.typeValueInField(Utils.getConfigPropertyByKey("passwordRegularUser"), "xpath.LoginFormPasswordField");
		actions.clickElement("xpath.LoginButtonLoginPage");
		actions.assertElementPresent("xpath.LogoutLink");
		Utils.LOG.info(Utils.getConfigPropertyByKey("usernameRegularUser") +" is logged in.");
	}

	public void loginAdminUser() {
		actions.waitForElementClickable("xpath.LoginLink",2);
		actions.clickElement("xpath.LoginLink");
		actions.assertElementPresent("xpath.LoginLogo");
		actions.clickElement("xpath.LoginFormEmailAddressField");
		actions.typeValueInField(Utils.getConfigPropertyByKey("usernameAdminUser"), "xpath.LoginFormEmailAddressField");
		actions.clickElement("xpath.LoginFormPasswordField");
		actions.typeValueInField(Utils.getConfigPropertyByKey("passwordAdminUser"), "xpath.LoginFormPasswordField");
		actions.clickElement("xpath.LoginButtonLoginPage");
		actions.assertElementPresent("xpath.LogoutLink");
		Utils.LOG.info(Utils.getConfigPropertyByKey("usernameAdminUser") +" is logged in.");
	}

}
