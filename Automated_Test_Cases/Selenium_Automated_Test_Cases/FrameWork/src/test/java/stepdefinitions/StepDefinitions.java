package stepdefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class StepDefinitions extends BaseStepDefinitions{
    UserActions actions = new UserActions();

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element){
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String field){
        actions.typeValueInField(value, field);
    }

    @Given("Wait for $element element")
    @When("Wait for $element element")
    @Then("Wait for $element element")
    public void waitForElement(String element) {
        actions.waitForElementVisible(element,2);
    }

    @Given("Wait to click on $element element")
    @When("Wait to click on $element element")
    @Then("Wait to click on $element element")
    public void waitForElementClickable(String element) {
        actions.waitForElementClickable(element,2);
    }

    @Given("Go to $url address")
    @When("Go to $url address")
    @Then("Go to $url address")
    public void goToUrl(String url) {
        actions.enterNewURL(url);
    }

    @Given("Confirm that $element element is present")
    @When("Confirm that $element element is present")
    @Then("Confirm that $element element is present")
    public void assertElementExist(String element) {
        actions.assertElementPresent(element);
    }



}
