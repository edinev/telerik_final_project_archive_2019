Meta:
@BDDTest

Narrative:
This is a complete collection of all test cases authored to evaluate
the functionality of the 'Social Network' application. The application
includes the following features to be tested:

Story:

Scenario: C173 PROFILE CREATION/LOGIN FORM
Given Confirm that xpath.SignUpLink element is present
When Click xpath.SignUpLink element
And Confirm that xpath.RegisterMaleRadioButton element is present
And Confirm that xpath.RegisterFemaleRadioButton element is present
And Confirm that xpath.RegisterFirstNameField element is present
And Confirm that xpath.RegisterLastNameField element is present
And Confirm that xpath.RegisterEmailField element is present
And Confirm that xpath.RegisterPasswordField element is present
And Confirm that xpath.ConfirmPasswordField element is present
And Confirm that xpath.RegisterSubmitButton element is present
Then Click xpath.RegisterFirstNameField element
And Type Jbehave in xpath.RegisterFirstNameField field
Then Click xpath.RegisterLastNameField element
And Type Bdd in xpath.RegisterLastNameField field
Then Click xpath.RegisterEmailField element
And Type jbehave.user@abv.bg in xpath.RegisterEmailField field
Then Click xpath.RegisterPasswordField element
And Type Qwerty1 in xpath.RegisterPasswordField field
Then Click xpath.ConfirmPasswordField element
And Type Qwerty1 in xpath.ConfirmPasswordField field
Then Click xpath.RegisterMaleRadioButton element
Then Click xpath.RegisterSubmitButton element

Scenario: C318 ELEVATED ACCESS - MODIFY A COMMENT, POST AND USER ACCOUNT
Given Go to http://localhost:8080/login address
When Wait to click on xpath.SignInPageUsernameInput element
And Click xpath.SignInPageUsernameInput element
And Type jbehave.user@abv.bg in xpath.SignInPageUsernameInput field
And Wait to click on xpath.SignInPagePasswordInput element
And Click xpath.SignInPagePasswordInput element
And Type Qwerty1 in xpath.SignInPagePasswordInput field
And Wait to click on xpath.SignInPageSubmitButton element
And Click xpath.SignInPageSubmitButton element
Then Wait for xpath.MainPageLogout element
Given Wait to click on xpath.WallPostTextArea element
When Click xpath.WallPostTextArea element
And Type Jbehave Bdd creates a post. Test Case C319. jBehave in xpath.WallPostTextArea field
Then Click xpath.WallPostSubmitButton element
Given Wait to click on xpath.WallCommentTextArea element
When Click xpath.WallCommentTextArea element
And Type Jbehave Bdd creates a comment. Test Case C319. jBehave in xpath.WallCommentTextArea field
Then Click xpath.WallAddCommentButton element
When Click xpath.LogoutButton element
And Go to http://localhost:8080/login address
Then Confirm that xpath.LoginFormEmailAddressField element is present
And Click xpath.SignInPageUsernameInput element
And Type test01@abv.bg in xpath.SignInPageUsernameInput field
And Wait to click on xpath.SignInPagePasswordInput element
And Click xpath.SignInPagePasswordInput element
And Type Qwerty1 in xpath.SignInPagePasswordInput field
And Wait to click on xpath.SignInPageSubmitButton element
And Click xpath.SignInPageSubmitButton element
And Wait for xpath.MainPageLogout element
Given Wait to click on xpath.NavBarAdminPanel element
When Click xpath.NavBarAdminPanel element
And Wait to click on xpath.CommentTableUpdateCommentButton element
And Click xpath.CommentTableUpdateCommentButton element
And Type Edited by admin. jBehave in xpath.CommentTableEditCommentElement field
And Click xpath.CommentTableEditSubmitButton element
And Click xpath.PostsTablesEditPostButton element
And Type Edited by admin. jBehave in xpath.PostsTableEditCommentElement field
And Click xpath.PostsTableEditSubmitButton element
