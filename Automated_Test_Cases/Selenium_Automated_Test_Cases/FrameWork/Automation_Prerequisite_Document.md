***Automation Prerequisites***
1. The 'Social Network' application must be running prior to test execution.
2. The Database of the application must be empty with the execption of the following user:
- Username: test01@abv.bg
- Password: Qwerty1
- First Name: User
- Last Name: Admin
- Level of access: ROLE_ADMIN
3. Post-execution the database must be cleared for next run.